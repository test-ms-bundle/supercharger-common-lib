def projectName = 'supercharger-common-lib'
def projectKey = 'io.digital.supercharger:common-lib'

pipeline {
    agent {
		label 'java'
	}
	stages {
        stage ('Compile') {
            steps {
               sh 'printenv'
               configFileProvider([configFile(fileId: 'nexus-settings.xml', variable: 'MAVEN_GLOBAL_SETTINGS')]) {
                    sh 'mvn -gs $MAVEN_GLOBAL_SETTINGS compile'
               }
            }
        }

		stage ('Code analysis') {
			steps {
			    configFileProvider([configFile(fileId: 'nexus-settings.xml', variable: 'MAVEN_GLOBAL_SETTINGS')]) {
			        sh 'mvn -gs $MAVEN_GLOBAL_SETTINGS checkstyle:check pmd:check spotbugs:check'
			    }
			}
		}

		stage ('Unit Tests') {
			steps {
			    configFileProvider([configFile(fileId: 'nexus-settings.xml', variable: 'MAVEN_GLOBAL_SETTINGS')]) {
			        sh 'mvn -gs $MAVEN_GLOBAL_SETTINGS test'
			    }
                jacoco(
                      execPattern: 'target/*.exec',
                      classPattern: 'target/classes',
                      sourcePattern: 'src/main/java',
                      exclusionPattern: 'src/test*'
                )
			}
		}

		stage ('Integration Tests') {
            steps {
                // Skip everything apart from the integration tests
                configFileProvider([configFile(fileId: 'nexus-settings.xml', variable: 'MAVEN_GLOBAL_SETTINGS')]) {
                    sh 'mvn -gs $MAVEN_GLOBAL_SETTINGS verify -Dskip.surefire.tests -Dspotbugs.skip=true -Dpmd.skip=true -Dcheckstyle.skip=true -Ddockerfile.skip'
                }
            }
        }

		stage ('SQ Code analysis & Test Coverage') {
			steps {
			     withSonarQubeEnv('sonar') {
			          configFileProvider([configFile(fileId: 'nexus-settings.xml', variable: 'MAVEN_GLOBAL_SETTINGS')]) {
                        sh 'mvn -gs $MAVEN_GLOBAL_SETTINGS sonar:sonar'
                      }
                  }
//				    script {
//                         if (env.BRANCH_NAME == 'master') {
//                             sh "mvn org.sonarsource.scanner.maven:sonar-maven-plugin:3.2:sonar -Dsonar.projectKey=${projectKey} -Dsonar.projectName=${projectName}"
//                         } else {
//                             sh "mvn org.sonarsource.scanner.maven:sonar-maven-plugin:3.2:sonar -Dsonar.projectKey=${projectKey} -Dsonar.projectName=${projectName} -Dsonar.branch.name=${env.BRANCH_NAME}" +
//                                 (env.CHANGE_TARGET == null ? "" : " -Dsonar.branch.target=${env.CHANGE_TARGET}")
//                         }
//                  }
            }
		}

		stage('SQ Quality Gate') {
		  steps {
			timeout(time: 5, unit: 'MINUTES') {
			  waitForQualityGate abortPipeline: true
			}
		  }
		}

        stage ('Deploy to Nexus') {
            steps {
                // If the version is set to SNAPSHOT than it will be deployed to Nexus Snapshots
                // If the version is set to RELEASE than it will be released
                configFileProvider([configFile(fileId: 'nexus-settings.xml', variable: 'MAVEN_GLOBAL_SETTINGS')]) {
                    sh 'mvn -gs $MAVEN_GLOBAL_SETTINGS deploy -DskipTests=true -Dskip.surefire.tests -Dfindbugs.skip -DaltSnapshotDeploymentRepository=nexus::${NEXUS_URL}/repository/maven-snapshots/ -DaltReleaseDeploymentRepository=nexus::${NEXUS_URL}/repository/maven-releases/'
                }
            }
        }
	}
	post {
        always {
            junit 'target/surefire-reports/**/*.xml'

            deleteDir() /* clean up our workspace */
        }
//         success {
//             office365ConnectorSend message: "Build Successful ${env.JOB_NAME} ${env.BUILD_NUMBER} commited by @${env.CHANGE_AUTHOR} (<${env.BUILD_URL}|Open>)",
//             webhookUrl: "${webhookUrl}", status: "SUCCESS", color: "00FF00"
//         }
//         failure {
//             office365ConnectorSend message: "Build FAILED ${env.JOB_NAME} ${env.BUILD_NUMBER} commited by @${env.CHANGE_AUTHOR} (<${env.BUILD_URL}|Open>)",
//             webhookUrl: "${webhookUrl}", status: "FAILED", color: "FF0000"
//         }
    }
}
