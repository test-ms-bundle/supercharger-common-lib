package io.digital.supercharger.common.dto.validation.password;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

/** Interface for validating the password. */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
@Constraint(validatedBy = {PasswordValidator.class})
public @interface Password {

  /**
   * Default message.
   *
   * @return the error message
   */
  String message() default "Password is invalid";

  /**
   * Grouping the validation.
   *
   * @return empty by default
   */
  Class<?>[] groups() default {};

  /**
   * Payload.
   *
   * @return empty by default
   */
  Class<? extends Payload>[] payload() default {};
}
