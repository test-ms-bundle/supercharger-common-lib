package io.digital.supercharger.common.dto.validation.password;

import io.digital.supercharger.common.util.PasswordValidatorUtil;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordValidator implements ConstraintValidator<Password, String> {

  @Override
  public void initialize(Password constraint) {
    // Initialize the CivilId validator
  }

  /**
   * Check if the civil Id is valid.
   *
   * @param password the request that is passed to the controller
   * @return boolean of true if valid
   */
  @Override
  public boolean isValid(String password, ConstraintValidatorContext context) {
    return PasswordValidatorUtil.validatePassword(password);
  }
}
