package io.digital.supercharger.common.dto.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Currency {
  AED("AED", "Emirati Dirham"),
  AUD("AUD", "Australian Dollar"),
  BHD("BHD", "Bahraini Dinar"),
  CAD("CAD", "Canadian dollar"),
  CHF("CHF", "Swiss Franc"),
  DKK("DKK", "Danish Krone"),
  EGP("EGP", "Egyptian Pound"),
  EURO("EURO", "EURO"),
  GBP("GBP", "Sterling Pound"),
  INR("INR", "Indian Rupee"),
  JOD("JOD", "Jordanian Dinar"),
  KWD("KWD", "Kuwaiti Dinar"),
  JPY("JPY", "Yen"),
  MYR("MYR", "Malaysian Ringits"),
  NOK("NOK", "Norwegian krone"),
  OMR("OMR", "Omani Riyal"),
  PHP("PHP", "Philippine Peso"),
  PKR("PKR", "Pakistan Rupee"),
  QAR("QAR", "Qatari Riyal"),
  SAR("SAR", "Saudi Riyal"),
  SEK("SEK", "Swedish Krona"),
  SGD("SGD", "Singapore Dollar"),
  TRY("TRY", "Turkish Lira"),
  USD("USD", "US Dollar");

  private final String value;
  private final String description;

  /**
   * From value to AccountType.
   *
   * @param type Account type
   * @return the AccountType
   */
  public static Currency fromValue(String type) {
    for (Currency currency : values()) {
      if (currency.getValue().equalsIgnoreCase(type)) {
        return currency;
      }
    }
    throw new IllegalArgumentException("Invalid Currency");
  }
}
