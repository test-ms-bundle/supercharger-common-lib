package io.digital.supercharger.common.dto.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CustomerStatusReason {
  CUSTOMER_IS_ACTIVE(1),
  INVALID_LOGIN_PASSWORD(2),
  INVALID_SITE_KEY_ANSWER(3),
  INVALID_OTP_OR_ACTIVATION_CODE(5),
  CUSTOMER_IS_LOCKED_BY_ADMIN(6),
  INVALID_CARD_PIN(9);

  private final int reasonCode;
}
