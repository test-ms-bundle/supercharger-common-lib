package io.digital.supercharger.common.dto.validation.iban;

import io.digital.supercharger.common.util.IbanValidator;
import java.io.IOException;
import javax.validation.ConstraintDefinitionException;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

@Component
public class IbanAnnotationValidator implements ConstraintValidator<Iban, String> {

  private IbanValidator ibanValidator;
  private String country;

  @Override
  public void initialize(Iban constraint) {
    // Initialize the Iban validator
    country = constraint.country();
    Resource resource = new ClassPathResource("countrySpecificIbanDetails.csv");
    try {
      ibanValidator = new IbanValidator(resource);

    } catch (IOException e) {
      throw new ConstraintDefinitionException("Country Specific Iban file not found", e);
    }
  }

  /**
   * Check if the Iban is valid.
   *
   * @param iban the request that is passed to the controller
   * @return boolean of true if valid
   */
  @Override
  public boolean isValid(String iban, ConstraintValidatorContext context) {

    if (iban.startsWith(country)) {
      return ibanValidator.validate(iban);
    } else {
      return false;
    }
  }
}
