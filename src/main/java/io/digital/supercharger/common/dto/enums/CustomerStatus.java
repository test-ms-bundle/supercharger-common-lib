package io.digital.supercharger.common.dto.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CustomerStatus {
  ACTIVE(1),
  LOCKED(2);

  private final int statusId;
}
