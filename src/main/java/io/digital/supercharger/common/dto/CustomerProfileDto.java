package io.digital.supercharger.common.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor(force = true)
@AllArgsConstructor
@Builder
public class CustomerProfileDto {
  private String id;
  private long cif;
  private boolean emailVerified;
  private String name;
  private String phoneNumber;
  private String username;
  private String firstName;
  private String lastName;
  private String picture;
  private String email;
}
