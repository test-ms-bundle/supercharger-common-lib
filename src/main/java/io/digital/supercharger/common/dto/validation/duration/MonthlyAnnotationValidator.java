package io.digital.supercharger.common.dto.validation.duration;

import static java.util.Objects.isNull;

import io.digital.supercharger.common.dto.enums.Duration;
import io.digital.supercharger.common.util.MonthlyValidator;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

@Component
public class MonthlyAnnotationValidator implements ConstraintValidator<Monthly, Duration> {

  /**
   * Check if the monthly duration is valid.
   *
   * @param duration the request that is passed to the controller
   * @return boolean of true if valid
   */
  @Override
  public boolean isValid(Duration duration, ConstraintValidatorContext context) {

    if (isNull(duration)) {
      return true;
    }

    return !ObjectUtils.isEmpty(duration) && MonthlyValidator.validateDuration(duration.getValue());
  }
}
