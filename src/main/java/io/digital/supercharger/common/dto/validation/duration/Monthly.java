package io.digital.supercharger.common.dto.validation.duration;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

/** Interface for validating the monthly duration. */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
@Constraint(validatedBy = {MonthlyAnnotationValidator.class})
public @interface Monthly {
  /**
   * Default message.
   *
   * @return the error message
   */
  String message() default "Monthly duration is invalid";

  /**
   * Grouping the validation.
   *
   * @return empty by default
   */
  Class<?>[] groups() default {};

  /**
   * Payload.
   *
   * @return empty by default
   */
  Class<? extends Payload>[] payload() default {};
}
