package io.digital.supercharger.common.dto.validation.iban;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
@Constraint(validatedBy = {IbanAnnotationValidator.class})
public @interface Iban {

  /**
   * Default message.
   *
   * @return the error message
   */
  String message() default "Iban is invalid";

  /**
   * Country selected.
   *
   * @return the error message
   */
  String country() default "KW";

  /**
   * Grouping the validation.
   *
   * @return empty by default
   */
  Class<?>[] groups() default {};

  /**
   * Payload.
   *
   * @return empty by default
   */
  Class<? extends Payload>[] payload() default {};
}
