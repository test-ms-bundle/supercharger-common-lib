package io.digital.supercharger.common.config;

import io.digital.supercharger.common.interceptor.RequestInterceptor;
import io.digital.supercharger.common.interceptor.security.AuthenticationInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class InterceptorConfig implements WebMvcConfigurer {

  @Bean
  AuthenticationInterceptor getAuthenticationInterceptor() {
    return new AuthenticationInterceptor();
  }

  @Bean
  RequestInterceptor getRequestInterceptor() {
    return new RequestInterceptor();
  }

  @Override
  public void addInterceptors(InterceptorRegistry registry) {
    registry.addInterceptor(getAuthenticationInterceptor());
    registry.addInterceptor(getRequestInterceptor());
  }
}
