package io.digital.supercharger.common.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.digital.supercharger.common.interceptor.HeaderRequestInterceptor;
import io.digital.supercharger.common.interceptor.LoggingInterceptor;
import io.digital.supercharger.common.util.RestUtil;
import java.util.concurrent.TimeUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestTemplateConfig {

  public static final long API_TIMEOUT_MILLI_SECONDS = TimeUnit.SECONDS.toMillis(60);

  @Autowired private CommonConfig commonConfig;

  /**
   * New Json with include non null and does not fail on unknown properties.
   *
   * @return objectMapper
   */
  @Bean
  @Primary
  public ObjectMapper nonNullObjectMapper() {
    return Jackson2ObjectMapperBuilder.json()
        .serializationInclusion(JsonInclude.Include.NON_NULL)
        .featuresToDisable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
        .build();
  }

  /**
   * Method for REST template generation.
   *
   * @param objectMapper the ObjectMapper use for converting objects
   * @return Generates REST template.
   */
  @Bean
  @Primary
  @Qualifier("genericRestTemplate")
  public RestTemplate genericRestTemplate(ObjectMapper objectMapper) {

    RestTemplate restTemplate = buildGenericRestTemplate(objectMapper);

    if (commonConfig.isInternalRequestLog()) {
      restTemplate
          .getInterceptors()
          .add(
              new LoggingInterceptor("Internal", true, true) {

                @Override
                protected String replaceSensitiveData(String reqBody) {
                  // can use this to mask any sensitive data
                  return reqBody;
                }
              });
    }

    return restTemplate;
  }

  /**
   * Method used to autowire a restTemplate with configuration for use with the ESB.
   *
   * @param objectMapper the objectmapper use for converting objects
   * @return restTemplateForESB
   */
  @Bean(name = "restTemplateForESB")
  public RestTemplate restTemplateForEsb(final ObjectMapper objectMapper) {
    RestTemplate restTemplate = buildGenericRestTemplate(objectMapper);

    // uncomment to add Basic Auth to the rest template
    RestUtil.addAuthentication(
        restTemplate, commonConfig.getEsbUsername(), commonConfig.getEsbPassword());

    restTemplate.getInterceptors().add(new HeaderRequestInterceptor());

    if (commonConfig.isEsbRequestLog()) {
      restTemplate
          .getInterceptors()
          .add(
              new LoggingInterceptor("ESB", true, true) {

                @Override
                protected String replaceSensitiveData(String reqBody) {
                  // can use this to mask any sensitive data
                  return reqBody;
                }
              });
    }

    return restTemplate;
  }

  /**
   * Helper method to set common attributes between different REST templates producers.
   *
   * @param objectMapper the objectmapper use for converting objects
   * @return REST template with default configurations like (timeout, interceptors, converters
   *     ...etc)
   */
  private RestTemplate buildGenericRestTemplate(final ObjectMapper objectMapper) {

    SimpleClientHttpRequestFactory clientConfig = new SimpleClientHttpRequestFactory();
    clientConfig.setConnectTimeout((int) API_TIMEOUT_MILLI_SECONDS);
    clientConfig.setReadTimeout((int) API_TIMEOUT_MILLI_SECONDS);

    RestTemplate restTemplate =
        new RestTemplate(new BufferingClientHttpRequestFactory(clientConfig));

    restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter(objectMapper));
    restTemplate.setErrorHandler(new DefaultResponseErrorHandler());

    return restTemplate;
  }
}
