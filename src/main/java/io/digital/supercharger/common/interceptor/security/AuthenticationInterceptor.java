package io.digital.supercharger.common.interceptor.security;

import io.digital.supercharger.common.config.CommonConfig;
import io.digital.supercharger.common.dto.CustomerProfileDto;
import io.digital.supercharger.common.exception.PermissionException;
import io.digital.supercharger.common.service.CustomerProfileService;
import io.digital.supercharger.common.util.RestUtil;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.lang.Nullable;
import org.springframework.util.StopWatch;
import org.springframework.util.StringUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * This Request Interceptor will validate the authentication token and loads the customer profile.
 */
@Log4j2
public class AuthenticationInterceptor extends HandlerInterceptorAdapter {

  public static final String REQUEST_ATTRIBUTE_CUSTOMER_PROFILE =
      "io.digital.supercharger.common.interceptor.security.customerProfile";

  private static final String REQUEST_ATTRIBUTE_STOPWATCH =
      "io.digital.supercharger.common.interceptor.security.stopwatch";

  @Autowired private CommonConfig commonConfig;

  @Autowired private CustomerProfileService customerProfileService;

  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
      throws Exception {

    LoginRequired login = getLoginAnnotation(handler);
    if (login == null) {
      return true;
    }

    String accessToken = request.getHeader(HttpHeaders.AUTHORIZATION);

    if (StringUtils.isEmpty(accessToken) && commonConfig.isLocalProfile()) {
      accessToken = "Bearer dummy-access-token";
    }

    if (StringUtils.isEmpty(accessToken)) {
      log.warn("Missing access token");
      throw new PermissionException("missing access token");
    }

    // get the token without the bearer header
    accessToken = RestUtil.getAuthorizationTokenWithoutBearer(accessToken);

    try {
      final StopWatch stopWatch = new StopWatch();
      stopWatch.start();

      var customerProfile = customerProfileService.authenticate(accessToken, login.offline());

      if (customerProfile == null) {
        log.warn("Customer profile is not found");
        throw new PermissionException("Customer profile is not found");
      }

      request.setAttribute(REQUEST_ATTRIBUTE_CUSTOMER_PROFILE, customerProfile);
      request.setAttribute(REQUEST_ATTRIBUTE_STOPWATCH, stopWatch);

    } catch (Exception ex) {
      log.warn("Unable to load customer profile", ex);
      throw new PermissionException("Unable to load customer profile");
    }

    return super.preHandle(request, response, handler);
  }

  /** Add the custom header with the refreshed token. */
  @Override
  public void afterCompletion(
      HttpServletRequest request,
      HttpServletResponse response,
      Object handler,
      @Nullable Exception ex) {

    CustomerProfileDto customerProfileDto =
        (CustomerProfileDto) request.getAttribute(REQUEST_ATTRIBUTE_CUSTOMER_PROFILE);
    String username = "NOT_FOUND";
    if (customerProfileDto != null) {
      username = customerProfileDto.getUsername();
    }

    StopWatch stopWatch = (StopWatch) request.getAttribute(REQUEST_ATTRIBUTE_STOPWATCH);
    if (stopWatch != null && stopWatch.isRunning()) {
      stopWatch.stop();
      log.info(
          "Auth after completion | url: {} | username: {} "
              + "| method: {} | status: {} | Total: {} seconds",
          request.getRequestURI(),
          username,
          request.getMethod(),
          response.getStatus(),
          stopWatch.getTotalTimeSeconds());
    }
  }

  /**
   * Get the LoginREquired handler if present.
   *
   * @param handler - the controller handler
   * @return LoginRequired annotation if present, otherwise null
   */
  private LoginRequired getLoginAnnotation(Object handler) {
    if (!(handler instanceof HandlerMethod)) {
      return null;
    }
    HandlerMethod handlerMethod = (HandlerMethod) handler;
    return handlerMethod.getMethod().getAnnotation(LoginRequired.class);
  }
}
