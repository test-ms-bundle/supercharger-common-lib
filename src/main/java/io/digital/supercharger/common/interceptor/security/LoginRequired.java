package io.digital.supercharger.common.interceptor.security;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marker interface to de-note that authentication is required for a particular controller method.
 * This interface is used by the AuthenticationInterceptor class. This interface provides support
 * for optionally indicating if the customer profile should be returned and if the authenticate
 * request is made for an async call to not refresh the auth token.
 *
 * @see io.digital.supercharger.common.interceptor.security.AuthenticationInterceptor
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface LoginRequired {

  /**
   * Specify if the call should be sent to backend or not.
   *
   * @return a boolean to indicate if the token should be sent to backend or not
   */
  boolean offline() default true;
}
