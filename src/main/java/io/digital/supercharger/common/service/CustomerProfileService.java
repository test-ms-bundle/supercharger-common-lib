package io.digital.supercharger.common.service;

import io.digital.supercharger.common.dto.CustomerProfileDto;

public interface CustomerProfileService {

  /**
   * Authenticate the customer and return their profile and the new token.
   *
   * @param token the token from frontend
   * @param offline The token would be verified with backend or not
   * @return SecurityTokenWithCustomerDto security token with customer dto
   */
  CustomerProfileDto authenticate(String token, boolean offline);
}
