package io.digital.supercharger.common.service;

import io.digital.supercharger.common.config.CommonConfig;
import io.digital.supercharger.common.dto.CustomerProfileDto;
import io.digital.supercharger.common.util.RestUtil;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Customer profile service is responsible for retrieving a customer profile from the Auth
 * microservice. The customer is retrieved using an authentication token
 */
@Service("customerProfileService")
@Log4j2
public class RestCustomerProfileService implements CustomerProfileService {

  @Qualifier("genericRestTemplate")
  private final RestTemplate restTemplate;

  private final CommonConfig configuration;

  @Autowired
  public RestCustomerProfileService(
      final RestTemplate restTemplate, final CommonConfig configuration) {
    this.restTemplate = restTemplate;
    this.configuration = configuration;
  }

  @Override
  public CustomerProfileDto authenticate(String token, boolean offline) {
    log.debug("Authenticating user...");
    var headers = RestUtil.getHttpHeaderWithBearerToken(token);
    headers.add("offline", String.valueOf(offline));
    ResponseEntity<CustomerProfileDto> response =
        restTemplate.postForEntity(
            configuration.getAuthServiceAuthUrl(),
            new HttpEntity<>("", headers),
            CustomerProfileDto.class);

    return response.getBody();
  }
}
