package io.digital.supercharger.common.util;

import static java.lang.Character.isUpperCase;
import static java.util.function.Function.identity;
import static org.springframework.util.ObjectUtils.isEmpty;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

/** Class that does complete IBAN validation, including applying country specific rules. */
@Component
public class IbanValidator {

  private static final int MINIMUM_IBAN_LENGTH = 8;
  private static final int MAXIMUM_IBAN_LENGTH = 34;

  private static final int MAX_ALPHANUMERIC_VALUE = 35;
  private static final long MAX = 999999999;

  private static final long MODULUS = 97;

  final Map<String, CountryValidator> countryValidators;

  /**
   * Validate list of iban.
   *
   * @param ibanDetails The iban details
   * @throws IOException If the file not found
   */
  @Autowired
  public IbanValidator(@Value("classpath:countrySpecificIbanDetails.csv") Resource ibanDetails)
      throws IOException {
    try (BufferedReader reader =
        new BufferedReader(
            new InputStreamReader(ibanDetails.getInputStream(), StandardCharsets.UTF_8))) {
      countryValidators =
          reader
              .lines()
              .map(line -> line.split(","))
              .map(
                  line ->
                      new CountryValidator(
                          line[0], Integer.parseInt(line[1]), Pattern.compile(line[2])))
              .collect(Collectors.toMap(CountryValidator::getCountryCode, identity()));
    }
  }

  /**
   * Basic rudimentary IBAN sanity check.
   *
   * <p>{@InheritDoc}
   */
  public boolean validate(final String iban) {
    if (isEmpty(iban)) {
      return false;
    }
    if (iban.length() < MINIMUM_IBAN_LENGTH || iban.length() > MAXIMUM_IBAN_LENGTH) {
      return false;
    }
    if (!isUpperCase(iban.charAt(0)) || !isUpperCase(iban.charAt(1))) {
      return false;
    }
    final String checkDigits = iban.substring(2, 4);
    if ("00".equals(checkDigits) || "01".equals(checkDigits) || "99".equals(checkDigits)) {
      return false;
    }
    if (!validateCountry(iban)) {
      return false;
    }
    return moduloCheck(iban);
  }

  private boolean moduloCheck(final String iban) {
    try {
      return calculateModulus(iban) == 1;
    } catch (IllegalArgumentException e) {
      return false;
    }
  }

  private int calculateModulus(final String iban) {
    final String reformattedCode = iban.substring(4) + iban.substring(0, 4);
    long total = 0;
    for (int i = 0; i < reformattedCode.length(); i++) {
      final int charValue = Character.getNumericValue(reformattedCode.charAt(i));
      if (charValue < 0 || charValue > MAX_ALPHANUMERIC_VALUE) {
        throw new IllegalArgumentException();
      }
      total = (charValue > 9 ? total * 100 : total * 10) + charValue;
      if (total > MAX) {
        total = total % MODULUS;
      }
    }
    return (int) (total % MODULUS);
  }

  /** checks if country is valid. */
  public boolean validateCountry(String iban) {
    final String countryCode = iban.substring(0, 2);
    if (!countryValidators.containsKey(countryCode)) {
      throw new IllegalArgumentException(
          "Unable to handle IBAN validator for country " + countryCode);
    }
    return countryValidators.get(countryCode).validate(iban);
  }

  @NoArgsConstructor
  @AllArgsConstructor
  private static class CountryValidator {
    String countryCode;
    int ibanLength;
    Pattern ibanPattern;

    public boolean validate(final String iban) {
      if (iban.length() != ibanLength) {
        return false;
      }
      return ibanPattern.matcher(iban).matches();
    }

    public String getCountryCode() {
      return countryCode;
    }
  }
}
