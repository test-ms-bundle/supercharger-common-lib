package io.digital.supercharger.common.util;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.support.BasicAuthenticationInterceptor;
import org.springframework.web.client.RestTemplate;

/** Generic REST utilities such as Auth and Headers. */
@Log4j2
public class RestUtil {

  public static final String AUTH_HEADER_BEARER = "Bearer ";

  /** Private Default constructor to prevent initialization from this class. */
  private RestUtil() {
    super();
  }

  /**
   * Add Basic authentication credentials to restTemplate.
   *
   * @param restTemplate for which auth to be added
   * @param username for the basic auth
   * @param password for the basic auth
   */
  public static void addAuthentication(
      RestTemplate restTemplate, String username, String password) {
    if (username == null) {
      return;
    }
    List<ClientHttpRequestInterceptor> interceptors = restTemplate.getInterceptors();

    interceptors = new ArrayList<>(interceptors);
    interceptors.removeIf(
        requestInterceptor -> requestInterceptor instanceof BasicAuthenticationInterceptor);

    interceptors.add(new BasicAuthenticationInterceptor(username, password));
    restTemplate.setInterceptors(interceptors);
  }

  /**
   * Return an authorization header token without the Bearer prefix.
   *
   * @param authenticationToken to put in the header
   * @return the full header with the "Bearer " prefix
   */
  public static String getAuthorizationTokenWithoutBearer(String authenticationToken) {
    return authenticationToken.replaceFirst(AUTH_HEADER_BEARER, "").trim();
  }

  /**
   * Return the http entity for the provided request.
   *
   * @param request - generic type for the request object
   * @return HttpEntity with the auth header connected
   */
  public static <T> HttpEntity<T> getHttpEntity(T request) {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    return new HttpEntity<>(request, headers);
  }

  /**
   * Return the http entity with bearer token for the provided request.
   *
   * @param request - generic type for the request object
   * @return HttpEntity with the auth header connected
   */
  public static <T> HttpEntity<T> getHttpEntityWithBearerToken(T request, String accessToken) {
    return new HttpEntity<>(request, getHttpHeaderWithBearerToken(accessToken));
  }

  /**
   * Return the Http Header with Json and Bearer token.
   *
   * @param accessToken The Access Token
   * @return Http Headers
   * @see HttpHeaders
   */
  public static HttpHeaders getHttpHeaderWithBearerToken(String accessToken) {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    headers.set(HttpHeaders.AUTHORIZATION, AUTH_HEADER_BEARER + accessToken);
    return headers;
  }

  /**
   * Return the Client ip Address if X-FORWARDED-FOR is set in header. if X-FORWARDED-FOR is set
   * (make sure that the API, Gateway setting this value).
   *
   * @param request The Http Request
   * @return The Client ip Address
   */
  public static String getClientIp(HttpServletRequest request) {

    String remoteAddr = "";

    if (request != null) {
      remoteAddr = request.getHeader("X-FORWARDED-FOR");
      if (remoteAddr == null || "".equals(remoteAddr)) {
        remoteAddr = request.getRemoteAddr();
        log.warn(
            "X-FORWARDED-FOR not set {}, this might lose the client real IP Address.", remoteAddr);
      }
    }

    return remoteAddr;
  }
}
