package io.digital.supercharger.common.util;

import java.util.regex.Pattern;

public class PasswordValidatorUtil {

  public static final Integer MIN = 8;
  public static final Integer MAX = 20;
  public static final String PAASSWORD_PATTERN = "(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9\\p{Punct}]+$";
  private static final Integer PASSWORD_SEQUENCE_COUNT = 3;
  private static final Integer PASSWORD_CONSECUTIVE_COUNT = 3;

  private PasswordValidatorUtil() {}

  /**
   * This method validates the password.
   *
   * @param password the password String
   * @return
   */
  public static boolean validatePassword(String password) {

    return !hasConsecutiveSeqOrRepeatedCharacters(password)
        && isValidLenghth(password)
        && isValidRegex(password);
  }

  private static boolean hasConsecutiveSeqOrRepeatedCharacters(String password) {
    return hasConsecutiveSeq(password) || hasRepeatedCharactors(password);
  }

  /**
   * This method check for the presence of sequential letters or numbers like (abc, 012, 123 ...).
   *
   * @param value the password String
   * @return
   */
  private static boolean hasConsecutiveSeq(String value) {
    if (value == null || value.trim().isEmpty()) {
      return false;
    }

    Integer maxSequenceCount = PASSWORD_SEQUENCE_COUNT;

    if (maxSequenceCount == 0 || maxSequenceCount == 1) {
      return false;
    }
    char[] valueCharArray = value.toCharArray();
    int asciiValue = 0;
    boolean isConSeq = false;
    int previousAsciiValue = 0;
    int numSeqcount = 1;
    for (char c : valueCharArray) {
      asciiValue = c;
      if (previousAsciiValue + 1 == asciiValue) {
        numSeqcount++;
        if (numSeqcount >= maxSequenceCount) {
          isConSeq = true;
          break;
        }
      } else {
        numSeqcount = 1;
      }
      previousAsciiValue = asciiValue;
    }
    return isConSeq;
  }

  /**
   * This method check for the presence of repeated letters or numbers.
   *
   * @param value the password String
   * @return
   */
  private static boolean hasRepeatedCharactors(String value) {

    if (value == null || value.trim().isEmpty()) {
      return false;
    }

    Integer maxConsecutiveCount = PASSWORD_CONSECUTIVE_COUNT;

    if (maxConsecutiveCount == 0 || maxConsecutiveCount == 1) {
      return false;
    }

    int countOfRepetition = 1;
    char character = value.charAt(0);
    for (int i = 1; i < value.length(); i++) {
      char nextCharacter = value.charAt(i);
      if (nextCharacter == character) {
        countOfRepetition++;
      } else {
        countOfRepetition = 1;
      }
      if (countOfRepetition == maxConsecutiveCount) {
        return true;
      }
      character = nextCharacter;
    }
    return false;
  }

  private static boolean isValidLenghth(String password) {

    return password.length() >= MIN && password.length() <= MAX;
  }

  private static boolean isValidRegex(String password) {

    return Pattern.matches(PAASSWORD_PATTERN, password);
  }
}
