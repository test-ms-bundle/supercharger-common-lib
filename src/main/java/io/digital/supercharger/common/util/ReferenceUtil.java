package io.digital.supercharger.common.util;

import java.nio.ByteBuffer;
import java.util.UUID;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.Validate;

/**
 * Utilities for reference manipulation, mostly UUID related references that can be used on URL and
 * path links.
 */
public class ReferenceUtil {

  private ReferenceUtil() {}

  /**
   * This method takes a string representation of a UUID and returns a base64 encoded ShortRef.
   *
   * @param uuidStr - the uuid from the database
   * @return encoded shortRef
   */
  public static String uuidToShortRef(String uuidStr) {
    Validate.notNull(uuidStr);

    UUID uuid = UUID.fromString(uuidStr);
    ByteBuffer bb = ByteBuffer.wrap(new byte[16]);
    bb.putLong(uuid.getMostSignificantBits());
    bb.putLong(uuid.getLeastSignificantBits());

    return Base64.encodeBase64URLSafeString(bb.array());
  }

  /**
   * This method takes a shortRef and returns.
   *
   * @param shortRef - the shortRef from the client
   * @return Identifier
   */
  public static UUID uuidFromShortRef(String shortRef) {
    Validate.notNull(shortRef);
    byte[] bytes = Base64.decodeBase64(shortRef);
    ByteBuffer bb = ByteBuffer.wrap(bytes);

    return new UUID(bb.getLong(), bb.getLong());
  }
}
