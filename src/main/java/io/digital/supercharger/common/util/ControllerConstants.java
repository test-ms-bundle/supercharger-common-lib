package io.digital.supercharger.common.util;

public class ControllerConstants {

  public static final String API = "/api";
  public static final String PAGE = "/page";
  public static final String PUBLIC_PATH = "/public"; // NOSONAR
  public static final String PUBLIC_API = PUBLIC_PATH + API;
  public static final String PUBLIC_PAGE = PAGE;
  public static final String BAD_REQUEST_ERROR_MESSAGE =
      "Bad Request error received from the client";
  public static final String NOT_FOUND_ERROR_MESSAGE =
      "The server has not found anything matching the Request-URI.";
  public static final String INTERNAL_SERVER_ERROR_MESSAGE = "Internal Server Error";
  public static final String INTERNAL = "Internal API";
  public static final String EXTERNAL = "External Api";

  private ControllerConstants() {
    // privaye
  }
}
