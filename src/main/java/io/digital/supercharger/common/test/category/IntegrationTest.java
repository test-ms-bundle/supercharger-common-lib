package io.digital.supercharger.common.test.category;

/**
 * A marker interface to use for the JUnit category annotation e.g.
 * {@literal @}Category(IntegrationTest.class)
 */
public interface IntegrationTest {}
