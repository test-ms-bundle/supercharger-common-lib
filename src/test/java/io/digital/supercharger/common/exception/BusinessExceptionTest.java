package io.digital.supercharger.common.exception;

import static io.digital.supercharger.common.exception.BusinessExceptionType.BET_0001;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;

class BusinessExceptionTest {
  @Test
  void testException() {
    try {
      throw new BusinessException(BET_0001);
    } catch (BusinessException r) {
      assertNotNull(r);
      assertEquals(BET_0001, r.errorCode);
      assertEquals(BET_0001.getMessage(), r.getMessage());
    }
  }

  @Test
  void testExceptionWithMessage() {
    final String testMessage = "Test Message";
    try {
      throw new BusinessException(BET_0001, testMessage);
    } catch (BusinessException r) {
      assertNotNull(r);
      assertEquals(BET_0001, r.errorCode);
      assertEquals(testMessage, r.getMessage());
    }
  }

  @Test
  void testExceptionWithEnumMissingMessage() {
    try {
      throw new BusinessException(TestCase.TEST1);
    } catch (BusinessException r) {
      assertNotNull(r);
      assertEquals(TestCase.TEST1, r.errorCode);
      assertEquals("TEST1", r.getMessage());
    }
  }

  @Test
  void testExceptionWithEnumMissingMessageWithMessage() {
    final String testMessage = "Test Message";
    try {
      throw new BusinessException(TestCase.TEST2, testMessage);
    } catch (BusinessException r) {
      assertNotNull(r);
      assertEquals(TestCase.TEST2, r.errorCode);
      assertEquals(testMessage, r.getMessage());
    }
  }

  private enum TestCase {
    TEST1,
    TEST2
  }
}
