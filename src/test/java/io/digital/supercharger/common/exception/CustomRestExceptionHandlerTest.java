package io.digital.supercharger.common.exception;

import static io.digital.supercharger.common.exception.BusinessExceptionType.BET_0001;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.digital.supercharger.common.api.CommonResponse;
import io.digital.supercharger.common.util.JsonUtil;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityNotFoundException;
import javax.validation.ConstraintViolationException;
import org.hibernate.validator.internal.engine.ConstraintViolationImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

@ExtendWith(MockitoExtension.class)
class CustomRestExceptionHandlerTest {

  private final CustomRestExceptionHandler customRestExceptionHandler =
      new CustomRestExceptionHandler();

  @Mock private WebRequest webrequest;
  @Mock private ConstraintViolationImpl<Error> violation;

  @Test
  void handleHttpMediaTypeNotSupported() throws IOException {
    HttpMediaTypeNotSupportedException httpMediaTypeNotSupportedException =
        mock(HttpMediaTypeNotSupportedException.class);
    HttpHeaders httpHeaders = mock(HttpHeaders.class);
    ResponseEntity<Object> objectResponseEntity =
        customRestExceptionHandler.handleHttpMediaTypeNotSupported(
            httpMediaTypeNotSupportedException, httpHeaders, HttpStatus.NOT_FOUND, webrequest);

    ApiError apiError = getApiError(objectResponseEntity.getBody());
    assertEquals(
        "null media type is not supported. Supported media types ar", apiError.getMessage());
    assertEquals(HttpStatus.UNSUPPORTED_MEDIA_TYPE, objectResponseEntity.getStatusCode());
  }

  @Test
  void handleMethodArgumentNotValid() throws IOException {
    MethodArgumentNotValidException methodArgumentNotValidException =
        mock(MethodArgumentNotValidException.class);
    BindingResult bindingResult = mock(BindingResult.class);
    when(methodArgumentNotValidException.getBindingResult()).thenReturn(bindingResult);
    List<FieldError> fieldErrors = new ArrayList<>();
    fieldErrors.add(new FieldError("object1", "field1", "Error"));
    fieldErrors.add(new FieldError("object2", "field2", "Error"));
    when(methodArgumentNotValidException.getBindingResult().getFieldErrors())
        .thenReturn(fieldErrors);
    List<ObjectError> objectErrors = new ArrayList<>();
    objectErrors.add(new ObjectError("object3", "error"));
    objectErrors.add(new ObjectError("object4", "error"));
    when(methodArgumentNotValidException.getBindingResult().getGlobalErrors())
        .thenReturn(objectErrors);
    HttpHeaders httpHeaders = mock(HttpHeaders.class);
    ResponseEntity<Object> objectResponseEntity =
        customRestExceptionHandler.handleMethodArgumentNotValid(
            methodArgumentNotValidException, httpHeaders, HttpStatus.NOT_FOUND, webrequest);

    ApiError apiError = getApiError(objectResponseEntity.getBody());
    assertEquals("Validation error", apiError.getMessage());
    assertEquals(4, apiError.getErrors().size());
    assertEquals(HttpStatus.BAD_REQUEST, objectResponseEntity.getStatusCode());
  }

  @Test
  void handleMissingServletRequestParameter() throws IOException {
    MissingServletRequestParameterException missingServletRequestParameterException =
        mock(MissingServletRequestParameterException.class);
    HttpHeaders httpHeaders = mock(HttpHeaders.class);
    ResponseEntity<Object> objectResponseEntity =
        customRestExceptionHandler.handleMissingServletRequestParameter(
            missingServletRequestParameterException, httpHeaders, HttpStatus.NOT_FOUND, webrequest);

    ApiError apiError = getApiError(objectResponseEntity.getBody());
    assertEquals("null parameter is missing", apiError.getMessage());
    assertEquals(HttpStatus.BAD_REQUEST, objectResponseEntity.getStatusCode());
  }

  @Test
  void handleConstraintViolation() throws IOException {

    ConstraintViolationException constraintViolationException =
        mock(ConstraintViolationException.class);

    // ConstraintViolation violation = mock(ConstraintViolation.class, Answers.RETURNS_MOCKS.get());
    // doReturn(Sets.newHashSet(violation)).when(constraintViolationException).getConstraintViolations();
    ResponseEntity<Object> objectResponseEntity =
        customRestExceptionHandler.handleConstraintViolation(constraintViolationException);

    ApiError apiError = getApiError(objectResponseEntity.getBody());
    assertEquals("Validation error", apiError.getMessage());
    assertEquals(HttpStatus.BAD_REQUEST, objectResponseEntity.getStatusCode());
  }

  @Test
  void handleMethodArgumentTypeMismatch() throws IOException {
    MethodArgumentTypeMismatchException methodArgumentTypeMismatchException =
        mock(MethodArgumentTypeMismatchException.class);
    ResponseEntity<Object> objectResponseEntity =
        customRestExceptionHandler.handleMethodArgumentTypeMismatch(
            methodArgumentTypeMismatchException, webrequest);

    ApiError apiError = getApiError(objectResponseEntity.getBody());
    assertEquals(
        "The parameter 'null' of value 'null' could not be converted to type ''",
        apiError.getMessage());
    assertEquals(HttpStatus.BAD_REQUEST, objectResponseEntity.getStatusCode());
  }

  @Test
  void handleMethodArgumentTypeMismatchNullRequiredType() throws IOException {
    MethodArgumentTypeMismatchException me = mock(MethodArgumentTypeMismatchException.class);
    when(me.getRequiredType()).thenReturn(null);
    ResponseEntity<Object> objectResponseEntity =
        customRestExceptionHandler.handleMethodArgumentTypeMismatch(me, webrequest);

    ApiError apiError = getApiError(objectResponseEntity.getBody());
    assertEquals(
        "The parameter 'null' of value 'null' could not be converted to type ''",
        apiError.getMessage());
    assertEquals(HttpStatus.BAD_REQUEST, objectResponseEntity.getStatusCode());
  }

  @Test
  void handleHttpClientExceptionWithBodyTest() throws IOException {
    String response =
        JsonUtil.toJson(
            new ApiError(
                HttpStatus.BAD_REQUEST,
                "Mobile number exists.")); // new ObjectMapper().writeValueAsString(new
    // ApiError(HttpStatus.BAD_REQUEST, "Mobile number
    // exists."));
    HttpClientErrorException errorResponse = mock(HttpClientErrorException.class);
    when(errorResponse.getResponseBodyAsString()).thenReturn(response);

    ResponseEntity<Object> objectResponseEntity =
        customRestExceptionHandler.handleHttpClientException(errorResponse);

    ApiError apiError = getApiError(objectResponseEntity.getBody());
    assertEquals("Mobile number exists.", apiError.getMessage());
    assertEquals(HttpStatus.BAD_REQUEST, objectResponseEntity.getStatusCode());
  }

  @Test
  void handleMethodArgumentTypeMismatchException() throws IOException {
    Assertions.assertThrows(
        IllegalArgumentException.class,
        () -> {
          customRestExceptionHandler.handleMethodArgumentTypeMismatch(null, webrequest);
        });
  }

  @Test
  void handlePermissionException() throws IOException {
    final String notFound = "You don't have permission!";
    ResponseEntity<Object> objectResponseEntity =
        customRestExceptionHandler.handleMicroservicePermissionException(
            new PermissionException(notFound));

    ApiError apiError = getApiError(objectResponseEntity.getBody());
    assertEquals(notFound, apiError.getMessage());
    assertEquals(HttpStatus.FORBIDDEN, objectResponseEntity.getStatusCode());
  }

  @Test
  void handleIllegalArgumentException() throws IOException {
    final String invalidArgument = "Argument is required";
    ResponseEntity<Object> objectResponseEntity =
        customRestExceptionHandler.handleIllegalArgumentException(
            new IllegalArgumentException(invalidArgument));

    ApiError apiError = getApiError(objectResponseEntity.getBody());
    assertEquals(invalidArgument, apiError.getMessage());
    assertEquals(HttpStatus.BAD_REQUEST, objectResponseEntity.getStatusCode());
  }

  @Test
  void handleEntityNotFound() throws IOException {
    final String notFound = "Not found?";
    ResponseEntity<Object> objectResponseEntity =
        customRestExceptionHandler.handleEntityNotFound(new EntityNotFoundException(notFound));

    ApiError apiError = getApiError(objectResponseEntity.getBody());
    assertEquals(notFound, apiError.getMessage());
    assertEquals(HttpStatus.NOT_FOUND, objectResponseEntity.getStatusCode());
  }

  @Test
  void handleAll() throws IOException {
    final String notFound = "Not found?";
    ResponseEntity<Object> objectResponseEntity =
        customRestExceptionHandler.handleAll(new EntityNotFoundException(notFound), webrequest);

    ApiError apiError = getApiError(objectResponseEntity.getBody());
    assertEquals(notFound, apiError.getMessage());
    assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, objectResponseEntity.getStatusCode());
  }

  @Test
  void handleIntegrationServiceException() throws IOException {
    final String test = "test";
    CommonResponse<Void> errorResponse = new CommonResponse<>();
    errorResponse.setErrorMessage(test);
    ResponseEntity<Object> objectResponseEntity =
        customRestExceptionHandler.handleIntegrationServiceException(
            new IntegrationServiceException(errorResponse));

    ApiError apiError = getApiError(objectResponseEntity.getBody());
    assertEquals(test, apiError.getMessage());
    assertEquals(HttpStatus.BAD_REQUEST, objectResponseEntity.getStatusCode());
  }

  @Test
  void testApiError() {
    ApiError error = new ApiError(HttpStatus.BAD_GATEWAY, new IOException("Gateway not found"));
    assertEquals("Gateway not found", error.getDebugMessage());
  }

  @Test
  void handleBusinessException() {
    final String businessException = "B Exception";
    ResponseEntity<Object> objectResponseEntity =
        customRestExceptionHandler.handleBusinessException(
            new BusinessException(BET_0001, businessException));

    ApiError apiError = (ApiError) objectResponseEntity.getBody();
    assertNotNull(apiError);
    assertEquals(BET_0001, apiError.getErrorCode());
    assertEquals(businessException, apiError.getMessage());
    assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, objectResponseEntity.getStatusCode());
  }

  @Test
  void handleBusinessExceptionWithoutMessage() {
    ResponseEntity<Object> objectResponseEntity =
        customRestExceptionHandler.handleBusinessException(new BusinessException(BET_0001));

    ApiError apiError = (ApiError) objectResponseEntity.getBody();
    assertNotNull(apiError);
    assertEquals(BET_0001, apiError.getErrorCode());
    assertEquals(BET_0001.getMessage(), apiError.getMessage());
    assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, objectResponseEntity.getStatusCode());
  }

  private ApiError getApiError(Object body) throws IOException {
    ObjectMapper mapper = new ObjectMapper();
    return mapper.readValue(JsonUtil.toJson(body), ApiError.class);
  }
}
