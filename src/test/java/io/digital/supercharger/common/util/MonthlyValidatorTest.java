package io.digital.supercharger.common.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.digital.supercharger.common.dto.enums.Duration;
import java.io.IOException;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.junit.jupiter.api.Test;

class MonthlyValidatorTest {

  @Test
  void whenNullDuration_shouldValidateNotOK() {
    boolean result = MonthlyValidator.validateDuration(null);
    assertFalse(result);
  }

  @Test
  void whenIncorrectDuration_shouldValidateNotOK() {
    boolean result = MonthlyValidator.validateDuration(13);
    assertFalse(result);
    result = MonthlyValidator.validateDuration(50);
    assertFalse(result);
  }

  @Test
  void whenCorrectDuration_shouldValidateOK() {
    boolean result = MonthlyValidator.validateDuration(12);
    assertTrue(result);
    result = MonthlyValidator.validateDuration(24);
    assertTrue(result);
    result = MonthlyValidator.validateDuration(36);
    assertTrue(result);
    result = MonthlyValidator.validateDuration(48);
    assertTrue(result);
    result = MonthlyValidator.validateDuration(60);
    assertTrue(result);
  }

  @Test
  void shouldProperlyDeserializeDurationIntoJson() throws IOException {
    MonthlyDuration actual =
        new ObjectMapper().readValue("{\"duration\":" + "\"12\"" + "}", MonthlyDuration.class);

    assertEquals(Duration.TWELVE, actual.getDuration());
    assertEquals(Duration.TWELVE.getValue(), actual.getDuration().getValue());
  }

  @Test
  void shouldProperlySerializeDurationIntoJson() throws IOException {
    MonthlyDuration monthlyDuration = new MonthlyDuration(Duration.THIRTY_SIX);
    String actual = new ObjectMapper().writeValueAsString(monthlyDuration);

    final ObjectNode node = new ObjectMapper().readValue(actual, ObjectNode.class);
    assertEquals(36, node.get("duration").asInt());
  }

  @NoArgsConstructor(force = true)
  @AllArgsConstructor
  @Getter
  @EqualsAndHashCode
  @ToString
  private static class MonthlyDuration {

    @JsonSerialize(using = MonthlyValidator.Serializer.class)
    @JsonDeserialize(using = MonthlyValidator.Deserializer.class)
    private final Duration duration;
  }
}
