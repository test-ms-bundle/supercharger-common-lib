package io.digital.supercharger.common.util;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

@ExtendWith(MockitoExtension.class)
class IbanValidatorTest {

  private IbanValidator ibanValidator;

  @BeforeEach
  void setup() throws IOException {
    Resource resource = new ClassPathResource("countrySpecificIbanDetails.csv");
    ibanValidator = new IbanValidator(resource);
  }

  @Test
  void whenNullIban_shouldValidateNotOK() {
    boolean result = ibanValidator.validate(null);
    assertFalse(result);
  }

  @Test
  void whenShortIban_shouldValidateNotOK() {
    boolean result = ibanValidator.validate("ABC");
    assertFalse(result);
  }

  @Test
  void whenLongIban_shouldValidateNotOK() {
    boolean result = ibanValidator.validate("ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ");
    assertFalse(result);
  }

  @Test
  void whenInvalidCheckDigit_shouldValidateNotOK() {
    boolean result = ibanValidator.validate("KW81CBKU0000000000001234560102");
    assertFalse(result);
  }

  @Test
  void whenValidCheckDigit_shouldValidateOK() {

    boolean result = ibanValidator.validate("KW81CBKU0000000000001234560101");
    assertTrue(result);
  }

  @Test
  void whenLowerCaseCountryCode_shouldValidateNotOK() {
    boolean result = ibanValidator.validate("kw81CBKU0000000000001234560101");
    assertFalse(result);
  }

  @Test
  void whenNotAlphanumericCharacter_shouldValidateNotOK() {
    boolean result = ibanValidator.validate("KW81@BKU0000000000001234560101");
    assertFalse(result);
  }

  @Test
  void whenCheckDigits00_shouldValidateNotOK() {
    boolean result = ibanValidator.validate("KW00CBKU0000000000001234560101");
    assertFalse(result);
  }

  @Test
  void whenCheckDigits01_shouldValidateNotOK() {
    boolean result = ibanValidator.validate("KW01CBKU0000000000001234560101");
    assertFalse(result);
  }

  @Test
  void whenCheckDigits99_shouldValidateNotOK() {
    boolean result = ibanValidator.validate("KW99CBKU0000000000001234560101");
    assertFalse(result);
  }

  @Test
  void whenInvalidCountrySpecificIban_shouldValidateNotOk() {

    boolean result = ibanValidator.validate("NL32ABNA0417164300123");
    assertFalse(result);
  }

  @Test
  void whenValidIbanKW_shouldValidateOK() {
    assertTrue(ibanValidator.validateCountry("KW81CBKU0000000000001234560101"));
  }

  @Test
  void whenValidIbanNL_shouldValidateOK() {
    assertTrue(ibanValidator.validateCountry("NL91ABNA0417164300"));
  }

  @Test
  void whenInValidIbanNL_shouldValidateNotOK() {
    assertFalse(ibanValidator.validateCountry("NL32ABNA0417164300123"));
  }

  @Test
  void whenValidIbanForUnknownCountry_shouldThrowException() {
    Assertions.assertThrows(
        IllegalArgumentException.class,
        () -> {
          ibanValidator.validateCountry("ZZ34CBKU0000000000001234560101");
        });
  }
}
