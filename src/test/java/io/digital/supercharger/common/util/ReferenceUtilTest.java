package io.digital.supercharger.common.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ReferenceUtilTest {

  private static final String SHORT_REF = "NA5Cfkq_TUWGQnMif1vDpQ";

  private static final String UID = "340e427e-4abf-4d45-8642-73227f5bc3a5";

  private String uuid;

  @BeforeEach
  void init() {
    uuid = UUID.randomUUID().toString();
  }

  @Test
  void testUuidToShortRef() {
    String ref = ReferenceUtil.uuidToShortRef(uuid);
    assertNotNull(ref);
    UUID uid = ReferenceUtil.uuidFromShortRef(ref);
    assertNotNull(uid);
    assertEquals(uuid, uid.toString());
  }

  @Test
  void testUuidFromQCode() {
    UUID id = ReferenceUtil.uuidFromShortRef(SHORT_REF);
    assertNotNull(id);
    assertEquals(UID, id.toString());
  }
}
