package io.digital.supercharger.common.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import io.digital.supercharger.common.interceptor.LoggingInterceptor;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpEntity;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.support.BasicAuthenticationInterceptor;
import org.springframework.web.client.RestTemplate;

@ExtendWith(MockitoExtension.class)
class RestUtilTest {

  @Mock RestTemplate genericRestTemplate;
  @Mock LoggingInterceptor logInterceptor;

  @Captor private ArgumentCaptor<List<ClientHttpRequestInterceptor>> captor;

  @Test
  void testRestUtilNullUsername() {
    RestUtil.addAuthentication(genericRestTemplate, null, "test");

    verify(genericRestTemplate, times(0)).getInterceptors();
  }

  @Test
  void testRestUtil() {

    when(genericRestTemplate.getInterceptors()).thenReturn(new ArrayList<>());

    RestUtil.addAuthentication(genericRestTemplate, "test", "test");

    /*
    ArgumentCaptor<EmailMessage> argument = ArgumentCaptor.forClass(EmailMessage.class);
    use this if you want to verify methods are called the order listed inside the verification block
    create inOrder object passing any mocks that need to be verified in order
    InOrder inOrder = inOrder(emailService);
    inOrder.verify(emailService, times(1)).sendEmail(argument.capture());
    */
    verify(genericRestTemplate, times(1)).setInterceptors(captor.capture());

    List<ClientHttpRequestInterceptor> interceptors = captor.getValue();

    List<ClientHttpRequestInterceptor> filtered =
        interceptors.stream()
            .filter(
                i -> {
                  return (i instanceof BasicAuthenticationInterceptor);
                })
            .collect(Collectors.toList());

    assertEquals(1, filtered.size());

    assertEquals(1, interceptors.size());
  }

  @Test
  void testRestUtilRemoveExistingAuthInterceptor() {

    final List<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();

    interceptors.add(new BasicAuthenticationInterceptor("user", "pass"));
    interceptors.add(logInterceptor);

    when(genericRestTemplate.getInterceptors()).thenReturn(interceptors);

    RestUtil.addAuthentication(genericRestTemplate, "test", "test");

    verify(genericRestTemplate, times(1)).setInterceptors(captor.capture());

    List<ClientHttpRequestInterceptor> actualInterceptors = captor.getValue();

    List<ClientHttpRequestInterceptor> filtered =
        actualInterceptors.stream()
            .filter(
                i -> {
                  return (i instanceof BasicAuthenticationInterceptor);
                })
            .collect(Collectors.toList());

    assertEquals(1, filtered.size());

    assertEquals(2, actualInterceptors.size());
  }

  @Test
  void testGetHttpEntity() {

    HttpEntity<String> entity = RestUtil.getHttpEntity("Test");

    assertEquals("Test", entity.getBody());
  }

  @Test
  void testGetBearerHttpEntity() {

    HttpEntity<String> entity = RestUtil.getHttpEntityWithBearerToken("Test", "access token");

    assertEquals("Test", entity.getBody());
  }

  @Test
  void testClientIpWithoutXForwardedFor() {
    HttpServletRequest mockedRequest = Mockito.mock(HttpServletRequest.class);
    when(mockedRequest.getRemoteAddr()).thenReturn("127.0.0.1");
    String ipAddress = RestUtil.getClientIp(mockedRequest);
    assertEquals("127.0.0.1", ipAddress);
  }

  @Test
  void testClientIpWithXForwardedFor() {
    HttpServletRequest mockedRequest = Mockito.mock(HttpServletRequest.class);
    when(mockedRequest.getHeader("X-FORWARDED-FOR")).thenReturn("127.1.1.1");
    String ipAddress = RestUtil.getClientIp(mockedRequest);
    assertEquals("127.1.1.1", ipAddress);
  }
}
