package io.digital.supercharger.common.util;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;
import org.junit.jupiter.api.Test;

class LocalDateUtilTest {

  @Test
  void isDatePastCutoffTrue() {
    LocalDate localDate = LocalDate.now().withDayOfMonth(6);
    boolean datePastCutoff = LocalDateUtil.isDatePastCutoff(localDate, 5);
    assertTrue(datePastCutoff);
  }

  @Test
  void isDatePastCutoffFalse() {
    LocalDate localDate = LocalDate.now().withDayOfMonth(4);
    boolean datePastCutoff = LocalDateUtil.isDatePastCutoff(localDate, 5);
    assertFalse(datePastCutoff);
  }
}
