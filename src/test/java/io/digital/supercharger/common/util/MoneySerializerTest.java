package io.digital.supercharger.common.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.math.BigDecimal;
import lombok.Data;
import org.junit.jupiter.api.Test;

class MoneySerializerTest {
  @Test
  void jsonSerializationTest() throws Exception {
    MoneyBean m = new MoneyBean();
    m.setAmount(new BigDecimal("20.38392378232"));

    ObjectMapper mapper = new ObjectMapper();
    assertEquals("{\"amount\":20.383}", mapper.writeValueAsString(m));
  }

  @Data
  class MoneyBean {
    @JsonSerialize(using = MoneySerializer.class)
    private BigDecimal amount;
  }
}
