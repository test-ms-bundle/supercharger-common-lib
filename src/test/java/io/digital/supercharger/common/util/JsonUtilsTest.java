package io.digital.supercharger.common.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import lombok.Data;
import org.junit.jupiter.api.Test;

class JsonUtilsTest {

  private static final String JSON1 =
      "{\r\n\"title\": \"Sample Title\",\r\n\"name\": \"main_window\",\r\n\"width\": 500,\r\n\"height\": 500\r\n}";
  private static final String JSON2 = "{\"userId\": \"123456789\"}";
  private static final String JSON_FILE = "user.json";

  @Test
  void testUtils() throws IOException {

    InputStream json = JsonUtilsTest.class.getClassLoader().getResourceAsStream(JSON_FILE);
    User toObj = JsonUtil.fromJson(json, User.class);

    assertNotNull(toObj, "Should be processed to obj successfully but it is not !!!");

    String jsonStr = JsonUtil.toJson(toObj, true);

    assertNotNull(jsonStr, "Should be processed to json successfully but it is not !!!");

    boolean validJson = JsonUtil.isValidJsonSring(jsonStr);
    assertEquals(true, validJson);

    boolean nullJson = JsonUtil.isValidJsonSring(null);
    assertEquals(false, nullJson);

    boolean inValidJson = JsonUtil.isValidJsonSring("sdkjf{}");
    assertEquals(false, inValidJson);

    User requestObj = JsonUtil.fromJsonStringToObject(jsonStr, User.class);

    assertNotNull(requestObj, "Should be processed to requestObj successfully but it is not !!!");
  }

  @Test
  void testFromJsonString() throws IOException {

    User toObj = JsonUtil.fromJson(JSON2, User.class);
    assertNotNull(toObj);
    assertEquals("123456789", toObj.getUserId());
  }

  @Test
  void testFromByteToObject() throws IOException {

    User toObj = JsonUtil.fromJson(JSON2, User.class);
    assertNotNull(toObj);
    assertEquals("123456789", toObj.getUserId());
  }

  @Test
  void testFromJsonStringReader() throws IOException {

    User toObj = JsonUtil.fromJson(new StringReader(JSON2), User.class);
    assertNotNull(toObj);
    assertEquals("123456789", toObj.getUserId());
  }

  @Test
  void testFromJsonInputStreamTypeReference() throws IOException {
    InputStream json = JsonUtilsTest.class.getClassLoader().getResourceAsStream(JSON_FILE);

    Map<String, Object> obj = JsonUtil.fromJson(json, new TypeReference<Map<String, Object>>() {});
    assertNotNull(obj);

    assertEquals(2, obj.size());
    assertNotNull(obj.get("userId"));
  }

  @Test
  void testFromJsonStringToMap() throws IOException {

    Map<String, Object> obj = JsonUtil.fromJson(JSON1);
    assertNotNull(obj);

    assertEquals(4, obj.size());
    assertNotNull(obj.get("title"));
  }

  @Test
  void testFromJsonStreamToMap() throws IOException {
    InputStream json = JsonUtilsTest.class.getClassLoader().getResourceAsStream(JSON_FILE);
    Map<String, Object> obj = JsonUtil.fromJson(json);
    assertNotNull(obj);

    assertEquals(2, obj.size());
    assertNotNull(obj.get("userId"));
  }

  @Test
  void testToJson() throws IOException {
    InputStream json = JsonUtilsTest.class.getClassLoader().getResourceAsStream(JSON_FILE);
    Map<String, Object> obj = JsonUtil.fromJson(json);
    assertNotNull(obj);
    User user = new User();

    for (String key : obj.keySet()) {
      this.callSetter(user, key, obj.get(key));
    }

    String prettyJson = JsonUtil.toJson(user, true);
    String compressedJson = JsonUtil.toJson(user, false);
    assertNotNull(prettyJson);
    assertNotNull(compressedJson);
    assertTrue(prettyJson.length() > compressedJson.length());

    user = JsonUtil.fromJson(compressedJson, User.class);

    for (String key : obj.keySet()) {
      Object value = this.callGetter(user, key);
      assertEquals(obj.get(key), value);
    }
    assertNotNull(user.getUserId());
    assertNotNull(user.getUsername());
  }

  /**
   * @param obj
   * @param fieldName
   * @param value
   */
  private void callSetter(Object obj, String fieldName, Object value) {
    PropertyDescriptor pd;
    try {
      pd = new PropertyDescriptor(fieldName, obj.getClass());
      pd.getWriteMethod().invoke(obj, value);
    } catch (IntrospectionException
        | IllegalAccessException
        | IllegalArgumentException
        | InvocationTargetException e) {
      e.printStackTrace();
    }
  }

  /**
   * @param obj
   * @param fieldName
   */
  private Object callGetter(Object obj, String fieldName) {
    PropertyDescriptor pd;
    Object value = null;
    try {
      pd = new PropertyDescriptor(fieldName, obj.getClass());
      value = pd.getReadMethod().invoke(obj);
    } catch (IntrospectionException
        | IllegalAccessException
        | IllegalArgumentException
        | InvocationTargetException e) {
      e.printStackTrace();
    }
    return value;
  }

  @Data
  @JsonInclude(JsonInclude.Include.NON_NULL)
  public static class User implements Serializable {

    private static final long serialVersionUID = 1717942839072883019L;

    private String userId;

    private String username;
  }
}
