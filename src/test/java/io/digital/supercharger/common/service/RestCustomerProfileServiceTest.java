package io.digital.supercharger.common.service;

import static io.digital.supercharger.common.util.RestUtil.AUTH_HEADER_BEARER;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import io.digital.supercharger.common.config.CommonConfig;
import io.digital.supercharger.common.dto.CustomerProfileDto;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@ExtendWith(MockitoExtension.class)
class RestCustomerProfileServiceTest {

  private static final String TOKEN = "SecurityToken123456699";

  @Mock private RestTemplate template;
  @Mock private CommonConfig commonConfig;
  private MultiValueMap<String, String> headers;
  private RestCustomerProfileService service;
  @Captor private ArgumentCaptor<HttpEntity<Object>> captor;

  @BeforeEach
  void setUp() {
    headers = new LinkedMultiValueMap<>();
    headers.put("offline", Collections.singletonList("true"));
    service = new RestCustomerProfileService(template, commonConfig);
  }

  @Test
  void testAuthenticate() {

    CustomerProfileDto secToken = new CustomerProfileDto();
    secToken.setUsername("username");
    ResponseEntity<CustomerProfileDto> response =
        new ResponseEntity<>(secToken, headers, HttpStatus.OK);

    final String url = "http://test/api/auth";

    when(commonConfig.getAuthServiceAuthUrl()).thenReturn(url);

    doReturn(response).when(template).postForEntity(eq(url), any(), eq(CustomerProfileDto.class));

    CustomerProfileDto customerProfileDto = service.authenticate(TOKEN, false);
    assertNotNull(customerProfileDto);
    assertEquals("username", customerProfileDto.getUsername());

    verify(commonConfig, times(1)).getAuthServiceAuthUrl();
    verify(template, times(1))
        .postForEntity(any(String.class), captor.capture(), eq(CustomerProfileDto.class));

    verifyHttpHeaders();
  }

  private void verifyHttpHeaders() {
    HttpEntity<Object> entity = captor.getValue();
    assertNotNull(entity);
    assertEquals(MediaType.APPLICATION_JSON, entity.getHeaders().getContentType());
    List<String> authValues = entity.getHeaders().get(HttpHeaders.AUTHORIZATION);
    assertEquals(1, Objects.requireNonNull(authValues).size());
    assertEquals(AUTH_HEADER_BEARER + TOKEN, authValues.get(0));
  }
}
