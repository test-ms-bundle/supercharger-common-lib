/** */
package io.digital.supercharger.common.config;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import io.digital.supercharger.common.interceptor.security.AuthenticationInterceptor;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import springfox.documentation.spring.web.plugins.Docket;

@ExtendWith(SpringExtension.class)
@WebAppConfiguration
@ContextConfiguration
@Tag("IntegrationTest")
class SpringConfigurationTest {

  @Autowired
  @Qualifier("genericRestTemplate")
  private RestTemplate restTemplate;

  @Autowired private CommonConfig configuration;

  @Autowired private AuthenticationInterceptor authenticationInterceptor;

  @Autowired private Docket api;

  @Test
  void testLoadConfigurations() {
    assertNotNull(restTemplate, "Generic RestTemplate is null");
    assertNotNull(configuration, "Configuration bean is null");
    assertNotNull(configuration.getAuthServiceAuthUrl());
    assertNotNull(configuration.getAuthServiceCustomerUrl());
    assertNotNull(configuration.getAuthServiceCustomerUrl());
    assertNotNull(configuration.getEsbUrl());
    assertFalse(configuration.isDevProfile());
    assertFalse(configuration.isLocalProfile());
    assertNotNull(authenticationInterceptor, "Auth interceptor is null");
    assertNotNull(api, "Swagger Docket is null");
  }

  @Configuration
  @EnableWebMvc
  @ComponentScan("io.digital.supercharger.common")
  public static class SpringConfig implements WebMvcConfigurer {}
}
