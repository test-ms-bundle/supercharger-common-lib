package io.digital.supercharger.common.config;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.client.RestTemplate;

@ExtendWith(MockitoExtension.class)
class RestTemplateCommonConfigTest {

  @Mock CommonConfig commonConfig;

  @Mock ObjectMapper objectMapper;

  @InjectMocks RestTemplateConfig restConfig;

  @Test
  void getRestTemplate() {

    RestTemplate template = restConfig.genericRestTemplate(objectMapper);

    assertNotNull(template);
  }

  @Test
  void getEsbRestTemplateConfigsDisableLog() {

    RestTemplate template = restConfig.restTemplateForEsb(objectMapper);

    assertNotNull(template.getInterceptors());
    assertEquals(1, template.getInterceptors().size());

    assertNotNull(template.getErrorHandler());

    assertNotNull(template.getMessageConverters());
  }

  @Test
  void getRestTemplateConfigsDisableLog() {

    RestTemplate template = restConfig.genericRestTemplate(objectMapper);

    assertNotNull(template.getInterceptors());
    assertEquals(0, template.getInterceptors().size());
  }

  @Test
  void getEsbRestTemplateConfigsEnableLog() {

    when(commonConfig.isEsbRequestLog()).thenReturn(true);

    RestTemplate template = restConfig.restTemplateForEsb(objectMapper);

    assertNotNull(template.getInterceptors());
    assertEquals(2, template.getInterceptors().size());

    assertNotNull(template.getErrorHandler());

    assertNotNull(template.getMessageConverters());
  }

  @Test
  void getRestTemplateConfigsEnableLog() {

    when(commonConfig.isInternalRequestLog()).thenReturn(true);

    RestTemplate template = restConfig.genericRestTemplate(objectMapper);

    assertNotNull(template.getInterceptors());
    assertEquals(1, template.getInterceptors().size());
  }
}
