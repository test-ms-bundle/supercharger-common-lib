package io.digital.supercharger.common.annotation;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import io.digital.supercharger.common.dto.validation.password.PasswordValidator;
import javax.validation.ConstraintValidatorContext;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

class PasswordValidatorTest {

  @Mock ConstraintValidatorContext context;

  @Test
  void passwordValidatorTest() {
    PasswordValidator passwordValidator = new PasswordValidator();
    boolean valid = passwordValidator.isValid("Passw0rd", context);
    assertTrue(valid);
  }

  @Test
  void passwordValidatorTestFailHasSequence() {
    PasswordValidator passwordValidator = new PasswordValidator();
    boolean valid = passwordValidator.isValid("Pas123word", context);
    assertFalse(valid);
  }

  @Test
  void passwordValidatorTestFailHasRepeatedCharacters() {
    PasswordValidator passwordValidator = new PasswordValidator();
    boolean valid = passwordValidator.isValid("Paaaas3word", context);
    assertFalse(valid);
  }

  @Test
  void passwordValidatorTestFailTooLong() {
    PasswordValidator passwordValidator = new PasswordValidator();
    boolean valid = passwordValidator.isValid("Paas3word19385739204ndikfmdke", context);
    assertFalse(valid);
  }

  @Test
  void passwordValidatorTestFailTooShort() {
    PasswordValidator passwordValidator = new PasswordValidator();
    boolean valid = passwordValidator.isValid("P", context);
    assertFalse(valid);
  }

  @Test
  void passwordValidatorTestFailRegexNotMatched() {
    PasswordValidator passwordValidator = new PasswordValidator();
    boolean valid = passwordValidator.isValid("Paas word", context);
    assertFalse(valid);
  }
}
