package io.digital.supercharger.common.dto.validation.duration;

import static org.junit.jupiter.api.Assertions.assertTrue;

import io.digital.supercharger.common.dto.enums.Duration;
import org.junit.jupiter.api.Test;

class MonthlyAnnotationValidatorTest {

  private final MonthlyAnnotationValidator monthlyAnnotationValidator =
      new MonthlyAnnotationValidator();

  @Test
  void isValid_returnsTrueForNullValidation() {
    assertTrue(monthlyAnnotationValidator.isValid(null, null));
  }

  @Test
  void isValid() {
    assertTrue(monthlyAnnotationValidator.isValid(Duration.TWELVE, null));
  }
}
