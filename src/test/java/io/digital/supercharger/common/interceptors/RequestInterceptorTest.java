package io.digital.supercharger.common.interceptors;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

import io.digital.supercharger.common.interceptor.RequestInterceptor;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class RequestInterceptorTest {

  @Mock HttpServletRequest request;

  @Mock HttpServletResponse response;

  RequestInterceptor requestInterceptor;

  @BeforeEach
  void setup() {
    when(request.getHeader("Accept-Language")).thenReturn("ar");
    when(request.getHeader("channelId")).thenReturn("channel1");
    when(request.getHeader("User-Agent")).thenReturn("android");
    when(request.getHeader("channeltype")).thenReturn("channelType1");

    requestInterceptor = new RequestInterceptor();
  }

  @Test
  void whenPreHandle_thenSuccess() {

    var preHandle = requestInterceptor.preHandle(request, response, null);
    assertNotNull(preHandle);
  }
}
