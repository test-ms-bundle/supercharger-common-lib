package io.digital.supercharger.common.interceptors;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import io.digital.supercharger.common.interceptor.LoggingInterceptor;
import java.io.IOException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.mock.http.client.MockClientHttpResponse;

@ExtendWith(MockitoExtension.class)
class LoggingInterceptorTest {

  private static final String BODY = "Sample body text";

  @Mock ClientHttpRequestExecution execution;

  @Mock HttpRequest request;

  @Mock ClientHttpResponse response;

  LoggingInterceptor interceptor;

  /** Before test. */
  @BeforeEach
  void beforeTest() {

    interceptor = new LoggingInterceptor("Junit", true, true);
  }

  @Test
  void testIntercept() throws IOException {

    final HttpHeaders headers = new HttpHeaders();

    headers.add("Authorization", "Bearer 234df-0rjafoapsdfaosdfpiasdf");

    headers.add("Content-Type", "application/json");

    final ClientHttpResponse clientHttpResponse =
        new MockClientHttpResponse("Cool response message".getBytes(), HttpStatus.ACCEPTED);

    when(request.getHeaders()).thenReturn(headers);
    when(execution.execute(eq(request), any(byte[].class))).thenReturn(clientHttpResponse);

    var response = interceptor.intercept(request, BODY.getBytes(), execution);
    assertNotNull(response);
  }

  @Test
  void testInterceptResponseException() throws IOException {

    final HttpHeaders headers = new HttpHeaders();

    headers.add("Authorization", "Bearer 234df-0rjafoapsdfaosdfpiasdf");

    headers.add("Content-Type", "application/json");

    when(response.getBody()).thenThrow(new IOException("Can not get body"));
    when(request.getHeaders()).thenReturn(headers);
    when(execution.execute(eq(request), any(byte[].class))).thenReturn(response);

    var response = interceptor.intercept(request, BODY.getBytes(), execution);
    assertNotNull(response);
  }

  @Test
  void testInterceptNoLogging() throws IOException {

    LoggingInterceptor interceptor1 = new LoggingInterceptor("Junit", false, false);

    when(execution.execute(eq(request), any(byte[].class))).thenReturn(response);

    var response = interceptor1.intercept(request, BODY.getBytes(), execution);
    assertNotNull(response);

    verify(request, times(0)).getHeaders();
    verify(response, times(0)).getBody();
  }
}
