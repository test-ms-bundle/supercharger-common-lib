package io.digital.supercharger.common.interceptors;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import io.digital.supercharger.common.interceptor.HeaderRequestInterceptor;
import java.io.IOException;
import java.net.URI;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpRequest;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestExecution;

@ExtendWith(MockitoExtension.class)
class HeaderRequestInterceptorTest {

  private static final String ACCEPT_KEY = "Accept";

  @Mock ClientHttpRequestExecution execution;

  @Mock HttpRequest request;

  @Test
  void intercept() throws IOException {

    Request request = new Request();

    HeaderRequestInterceptor interceptor = new HeaderRequestInterceptor();

    interceptor.intercept(request, null, execution);

    assertNotNull(request.getHeaders());

    assertTrue(request.getHeaders().containsKey(ACCEPT_KEY));

    assertTrue(request.getHeaders().getAccept().contains(MediaType.APPLICATION_JSON));
  }

  private class Request implements HttpRequest {

    HttpHeaders headers = new HttpHeaders();

    @Override
    public HttpMethod getMethod() {
      return null;
    }

    @Override
    public HttpHeaders getHeaders() {
      return headers;
    }

    @Override
    public String getMethodValue() {
      return null;
    }

    @Override
    public URI getURI() {
      return null;
    }
  }
}
