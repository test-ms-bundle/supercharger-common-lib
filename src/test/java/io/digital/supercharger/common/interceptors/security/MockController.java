package io.digital.supercharger.common.interceptors.security;

import io.digital.supercharger.common.dto.CustomerProfileDto;
import io.digital.supercharger.common.interceptor.security.InternalAccess;
import io.digital.supercharger.common.interceptor.security.LoginRequired;
import io.swagger.annotations.ApiParam;
import java.math.BigDecimal;
import org.springframework.web.bind.annotation.RequestAttribute;
import springfox.documentation.annotations.ApiIgnore;

public class MockController {

  @LoginRequired
  public String getUsername(
      @ApiIgnore @ApiParam(hidden = true) @RequestAttribute(required = false)
          CustomerProfileDto customerProfile) {
    return "Omer Dawelbeit";
  }

  @LoginRequired
  public String getAddress() {
    return "74 Westlands Avenue";
  }

  @InternalAccess(authRequired = true, passProp = "auth.password", usernameProp = "auth.username")
  public BigDecimal calculateTotal() {
    return BigDecimal.ZERO;
  }

  public BigDecimal getTotalAmount() {

    return BigDecimal.ONE;
  }
}
