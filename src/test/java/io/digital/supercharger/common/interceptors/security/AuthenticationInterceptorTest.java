package io.digital.supercharger.common.interceptors.security;

import static io.digital.supercharger.common.interceptor.security.AuthenticationInterceptor.REQUEST_ATTRIBUTE_CUSTOMER_PROFILE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import io.digital.supercharger.common.config.CommonConfig;
import io.digital.supercharger.common.dto.CustomerProfileDto;
import io.digital.supercharger.common.exception.PermissionException;
import io.digital.supercharger.common.interceptor.security.AuthenticationInterceptor;
import io.digital.supercharger.common.interceptor.security.InternalAccess;
import io.digital.supercharger.common.interceptor.security.LoginRequired;
import io.digital.supercharger.common.service.CustomerProfileService;
import java.lang.reflect.Method;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpHeaders;
import org.springframework.util.StopWatch;
import org.springframework.web.method.HandlerMethod;

@ExtendWith(MockitoExtension.class)
class AuthenticationInterceptorTest {

  private static final String BEARER_TOKEN = "Bearer blasdfasdfasdf";
  private static final String ACCESS_TOKEN = "blasdfasdfasdf";
  @InjectMocks AuthenticationInterceptor interceptor;
  @Mock HttpServletRequest request;
  @Mock HttpServletResponse response;
  @Mock HandlerMethod method;
  @Mock LoginRequired loginRequired;
  @Mock CustomerProfileDto customer;
  @Mock CustomerProfileService service;
  @Mock CommonConfig commonConfig;
  Method refMethod;

  @BeforeEach
  void beforeTest() throws NoSuchMethodException {
    refMethod = MockController.class.getMethod("getUsername", CustomerProfileDto.class);
  }

  @Test
  void testInternalAccess() throws Exception {
    Method meth = MockController.class.getMethod("calculateTotal");
    assertNotNull(meth);
    InternalAccess access = meth.getAnnotation(InternalAccess.class);
    assertNotNull(access);
    assertTrue(access.authRequired());
    assertEquals("auth.username", access.usernameProp());
    assertEquals("auth.password", access.passProp());
  }

  @Test
  void testPreHandleNonHandlerMethod() throws Exception {
    this.interceptor.preHandle(request, response, new Object());

    verify(method, times(0)).getMethod();
  }

  @Test
  void testPreHandleEmptyToken() throws Exception {

    /** ******** Arrange ******** */
    when(method.getMethod()).thenReturn(refMethod);
    when(request.getHeader(HttpHeaders.AUTHORIZATION)).thenReturn(null);

    /** ******** Act ******** */
    assertThrows(
        PermissionException.class,
        () -> {
          this.interceptor.preHandle(request, response, method);
        });

    /** ******** Assert ******** */
    verify(method, times(1)).getMethod();
    verify(request, times(1)).getHeader(HttpHeaders.AUTHORIZATION);
    verify(service, times(0)).authenticate(anyString(), anyBoolean());
  }

  @Test
  void testPreHandleNoLogin() throws Exception {

    /** ******** Arrange ******** */
    final Method getTotalMethod = MockController.class.getMethod("getTotalAmount");
    when(method.getMethod()).thenReturn(getTotalMethod);

    /** ******** Act ******** */
    this.interceptor.preHandle(request, response, method);

    /** ******** Assert ******** */
    verify(method, times(1)).getMethod();
    verify(request, times(0)).getHeader(HttpHeaders.AUTHORIZATION);
  }

  @Test
  void testPreHandleIsLocalProfile() throws Exception {

    /** ******** Arrange ******** */
    when(commonConfig.isLocalProfile()).thenReturn(true);
    Method addressMethod = MockController.class.getMethod("getAddress");
    CustomerProfileDto customerProfileDto = new CustomerProfileDto();
    when(method.getMethod()).thenReturn(addressMethod);
    when(request.getHeader(HttpHeaders.AUTHORIZATION)).thenReturn("");
    when(service.authenticate(anyString(), eq(true))).thenReturn(customerProfileDto);

    /** ******** Act ******** */
    this.interceptor.preHandle(request, response, method);

    /** ******** Assert ******** */
    verify(method, times(1)).getMethod();
    verify(request, times(1)).getHeader(eq(HttpHeaders.AUTHORIZATION));
    verify(request, times(1))
        .setAttribute(eq(REQUEST_ATTRIBUTE_CUSTOMER_PROFILE), eq(customerProfileDto));
  }

  @Test
  void testPreHandleWithNull() throws Exception {

    /** ******** Arrange ******** */
    Method addressMethod = MockController.class.getMethod("getAddress");
    when(method.getMethod()).thenReturn(addressMethod);
    when(request.getHeader(HttpHeaders.AUTHORIZATION)).thenReturn(BEARER_TOKEN);
    when(service.authenticate(anyString(), eq(false))).thenReturn(null);

    /** ******** Act ******** */
    assertThrows(
        PermissionException.class,
        () -> this.interceptor.preHandle(request, response, method),
        "Unable to load customer profile");

    /** ******** Assert ******** */
    verify(method, times(1)).getMethod();
    verify(request, times(1)).getHeader(HttpHeaders.AUTHORIZATION);
    verify(request, times(0))
        .setAttribute(REQUEST_ATTRIBUTE_CUSTOMER_PROFILE, new CustomerProfileDto());
  }

  @Test
  void testPreHandleUserServiceException() throws Exception {

    /** ******** Arrange ******** */
    when(method.getMethod()).thenReturn(refMethod);
    when(request.getHeader(HttpHeaders.AUTHORIZATION)).thenReturn(BEARER_TOKEN);
    when(service.authenticate(anyString(), anyBoolean()))
        .thenThrow(new RuntimeException("Unable to retrieve customer"));

    /** ******** Act ******** */
    assertThrows(
        PermissionException.class,
        () -> this.interceptor.preHandle(request, response, method),
        "Unable to load customer profile");

    /** ******** Assert ******** */
    verify(method, times(1)).getMethod();
    verify(request, times(1)).getHeader(HttpHeaders.AUTHORIZATION);
    verify(service, times(1)).authenticate(ACCESS_TOKEN, true);
    verify(request, times(0)).setAttribute(anyString(), any());
  }

  @Test
  void testPreHandleNullCustomerProfile() throws Exception {

    /** ******** Arrange ******** */
    when(method.getMethod()).thenReturn(refMethod);
    when(request.getHeader(HttpHeaders.AUTHORIZATION)).thenReturn(BEARER_TOKEN);
    when(service.authenticate(anyString(), anyBoolean())).thenReturn(null);

    /** ******** Act ******** */
    assertThrows(
        PermissionException.class, () -> this.interceptor.preHandle(request, response, method));

    /** ******** Assert ******** */
    verify(method, times(1)).getMethod();
    verify(request, times(1)).getHeader(HttpHeaders.AUTHORIZATION);
    verify(service, times(1)).authenticate(ACCESS_TOKEN, true);
    verify(request, times(0)).setAttribute(anyString(), any());
  }

  @Test
  void testAfterCompletionNoToken() throws Exception {

    this.interceptor.afterCompletion(request, response, method, null);

    // InOrder order = inOrder(method, request, response);
    verify(request, times(1)).getAttribute(REQUEST_ATTRIBUTE_CUSTOMER_PROFILE);
    verify(response, times(0)).setHeader(anyString(), anyString());
  }

  @Test
  void testAfterCompletionWithToken() throws Exception {

    /** ******** Arrange ******** */
    when(request.getAttribute(REQUEST_ATTRIBUTE_CUSTOMER_PROFILE))
        .thenReturn(CustomerProfileDto.builder().username("test").build());
    StopWatch stopWatch = new StopWatch();
    stopWatch.start();
    when(request.getAttribute("io.digital.supercharger.common.interceptor.security.stopwatch"))
        .thenReturn(stopWatch);
    assertTrue(stopWatch.isRunning());
    this.interceptor.afterCompletion(request, response, method, null);

    verify(request, times(1)).getAttribute(REQUEST_ATTRIBUTE_CUSTOMER_PROFILE);

    assertFalse(stopWatch.isRunning());
  }
}
